/*
* Queue.cpp
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*/
#include <iostream>
#include "Queue.h"

using namespace std;

Queue :: Queue()
{
    front = -1;
    rear = -1;
}

bool Queue :: isFull()
{
    if(front == 0 && rear == SIZE - 1)
        return true;
    else
        return false;
}

bool Queue :: isEmpty()
{
    if(front == -1)
        return true;
    else
        return false;
}

void Queue :: enQueue(int element)
{
    if(Queue :: isFull())
    {
        cout << "QUEUE FULL!" << endl;
        return;
    }
    else
    {
        if(front == -1)
            front = 0;
        rear++;
        items[rear] = element;
    }
}

void Queue :: deQueue()
{
    int element;
    if(Queue :: isEmpty())
    {
        cout << "QUEUE EMPTY!" << endl;
        return;
    }
    else
    {
        element = items[front];
        if(front >= rear)
        {
            front = -1;
            rear = -1;
        }
        else
        {
            front++;
        }
        cout << "Deleted -> " << element << endl;
    }
}

void Queue :: display()
{
    if (Queue :: isEmpty())
    {
        cout << "QUEUE EMPTY!, NOTHING TO DISPLAY" << endl;
        return;
    }
    cout << "Queue : ";
    for (int i = front; i <= rear; i++)
    {
        cout << items[i] << " ";
    }
    cout << endl;
}