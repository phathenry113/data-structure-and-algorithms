/*
* circularQueue.h
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*
*/
#ifndef __CIRCULARQUEUE_H__
#define __CIRCULARQUEUE_H__

#define SIZE 10

class circularQueue
{
private:
    /* properties */
    int items[SIZE];
    int front;
    int rear;
public:
    /* class constructor */
    circularQueue();

    // @brief: check queue whether full or not?
    // @return: true or false
    bool isFull();

    // @brief: check queue whether empty or not?
    // @return: true or false
    bool isEmpty();

    // @brief: push a element into the queue
    // @param: value of element
    void enQueue(int);

    // @brief: pop a element from the queue
    // @param: none
    void deQueue();

    // @brief: display the elements of queue
    // @return: none
    void display();
};

#endif // CIRCULARQUEUE_H