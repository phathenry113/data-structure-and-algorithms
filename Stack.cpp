/*
* Stack.cpp
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*/
#include <iostream>
#include "Stack.h"

using namespace std;

Stack :: Stack()
{
    top = -1;
    count = 0;
}

bool Stack :: isFull()
{
    if(top == SIZE - 1)
        return true;
    else
        return false;
}

bool Stack :: isEmpty()
{
    if(top == -1)
        return true;
    else
        return false;
}

void Stack :: push(int element)
{
    if(Stack :: isFull())
    {
        cout << "STACK FULL!" << endl;
        return;
    }
    else
    {
        top++;
        items[top] = element;
    }
    count++;
}

void Stack :: pop()
{
    if(Stack :: isEmpty())
    {
        cout << "STACK EMPTY!" << endl;
        return;
    }
    else
    {
        cout << "Items popped -> " << items[top] << endl;
        top--;
    }
    count--;
}

void Stack :: display()
{
    if (Stack :: isEmpty())
    {
        cout << "STACK EMPTY!, NOTHING TO DISPLAY" << endl;
        return;
    }
    cout << "Stack : ";
    for (int i = 0; i < count; i++)
    {
        cout << items[i] << " ";
    }
    cout << endl;
}