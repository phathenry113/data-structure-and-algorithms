/*
* circularQueue.cpp
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*/
#include <iostream>
#include "circularQueue.h"

using namespace std;

circularQueue :: circularQueue()
{
    front = -1;
    rear = -1;
}

bool circularQueue :: isFull()
{
    if(front == 0 && rear == SIZE - 1)
        return true;
    if(front == rear + 1)
        return true;
    return false;
}

bool circularQueue :: isEmpty()
{
    if(front == -1)
        return true;
    else
        return false;
}

void circularQueue :: enQueue(int element)
{
    if(circularQueue :: isFull())
    {
        cout << "QUEUE FULL!" << endl;
        return;
    }
    else
    {
        if(front == -1)
            front = 0;
        rear = (rear + 1) % SIZE;
        items[rear] = element;
    }
}

void circularQueue :: deQueue()
{
    int element;
    if(circularQueue :: isEmpty())
    {
        cout << "QUEUE EMPTY!" << endl;
        return;
    }
    else
    {
        element = items[front];
        if(front == rear)
        {
            front = -1;
            rear = -1;
        }
        else
        {
            front = (front + 1) % SIZE;
        }
        cout << "Deleted -> " << element << endl;
    }
}

void circularQueue :: display()
{
    if (circularQueue :: isEmpty())
    {
        cout << "QUEUE EMPTY!, NOTHING TO DISPLAY" << endl;
        return;
    }
    cout << "circularQueue : ";
    for (int i = front; i != rear; i = (i + 1) % SIZE)
    {
        cout << items[i] << " ";
    }
    cout << endl;
}