/*
* Stack.h
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*/
#ifndef __STACK_H__
#define __STACK_H__

#define SIZE 10

class Stack
{
private:
    /* properties */
    int items[SIZE];
    int top;
    int count;
public:
    /* class constructor */
    Stack();

    // @brief: check stack whether full or not?
    // @return: true or false
    bool isFull();

    // @brief: check stack whether empty or not?
    // @return: true or false
    bool isEmpty();

    // @brief: push a element into the stack
    // @param: value of element
    void push(int);

    // @brief: pop a element from the stack
    // @param: none
    void pop();

    // @brief: display the elements of stack
    // @return: none
    void display();
};

#endif // STACK_H