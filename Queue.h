/*
* Queue.h
*   Created on: Nov 28, 2020
*   Author: PhatTS
*   Email: phathenry113@gmail.com
*
*/
#ifndef __QUEUE_H__
#define __QUEUE_H__

#define SIZE 10

class Queue
{
private:
    /* properties */
    int items[SIZE];
    int front;
    int rear;
public:
    /* class constructor */
    Queue();

    // @brief: check queue whether full or not?
    // @return: true or false
    bool isFull();

    // @brief: check queue whether empty or not?
    // @return: true or false
    bool isEmpty();

    // @brief: push a element into the queue
    // @param: value of element
    void enQueue(int);

    // @brief: pop a element from the queue
    // @param: none
    void deQueue();

    // @brief: display the elements of queue
    // @return: none
    void display();
};

#endif // QUEUE_H